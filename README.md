# Simple Shop api

This is api for Simple Shop app

## Configuration

Create `.env` file in main project directory and fill it with your port number:

 <code>
 PORT=1234 
 </code>

## How to run project

`git clone git@gitlab.com:mariuszsak/simple_shop_api.git`

`npm install`

`npm start`
