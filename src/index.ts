import * as express from 'express';
import * as dotenv from 'dotenv';


const app = express();
dotenv.config();

const port = process.env.PORT;

app.get('/', (req: express.Request, res: express.Response) => {
    res.status(200);
    res.send('All ok');
    res.end();
})

app.listen(port, (): void => {
    console.log('Server started at port ' + port);
})
